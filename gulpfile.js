var gulp = require('gulp');
var fs = require('fs');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');
var jsonlint = require('gulp-jsonlint');
var ucss = require('ucss');
var stylish = require('jshint-stylish');
var requirejs = require('requirejs');
var request = require('request');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

/**
 * SASS compile task
 */
gulp.task('sass', function () {
	gulp.src('./mysite/polls/assets/sass/**/*.scss')
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'compressed',
			errLogToConsole: false
		}))
		.on('error', function (err) {
			notify({ title: "SASS", subtitle: 'error' }).write(err);
			this.emit('end');
		})
		.pipe(gulp.dest('./mysite/polls/static/polls/css'))
		.pipe(notify({ title: 'SASS', subtitle: 'success', message: 'SASS compiled successfully' }));
});

gulp.task('js', function() {
  gulp.src('./mysite/polls/assets/js/**/*.js')
	.pipe(concat('scripts.js'))
    .pipe(gulp.dest('./mysite/polls/static/js'))
	.pipe(rename('scripts.min.js'))
    .pipe(uglify())
	.pipe(gulp.dest('./mysite/polls/static/js'))
});

gulp.task('default', ['js']);
