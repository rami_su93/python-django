const path = require('path');

module.exports = {
  mode: 'development',
  context: path.join( __dirname + '/mysite/polls/assets/js'),
  entry: './main.js',
  devtool: 'source-map',
  output: {
	  	path: path.resolve('mysite/polls/static/js'),
		publicPath: '/polls/static/js/',
  		filename: '[name].js',
  		chunkFilename: 'partial.[name].js'
  }
};
