define([
],
 function () {
	'use strict';

	/**
	 * App
	 * This is the initialization file for all scripts.
	 * @version 2.0.0
	 * @see {@link http://youmightnotneedjquery.com/}
	 * @exports app
	 */
	var app = {
		/**
		 * The initialization function for this module.
		 * @public
		 */
		init: function () {

			this._initText();


		},


		/**
		 * Function that initiates the ajax-form functionality.
		 * @private
		 */
		_initText: function () {
			console.log("bla");
		},


	};


	return app;
});
