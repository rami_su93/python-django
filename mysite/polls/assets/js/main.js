/**
 * main.js
 * Here we handle all dependencies (the actual starting point is in app.js).
 * @see {@link http://usejsdoc.org} for information about comments standard used.
 * @version 2.0.0
 */

define(['./app'], function (app) {
	'use strict';

	app.init();
});
